var express = require('express'),
	app = new express();
var port = process.env.PORT || 5000;
var host = process.env.HOST || "127.0.0.1";
var bodyParser = require('body-parser');

app.listen(port, () => console.log('Running on port 5000!'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(err, req, res, next) {
  // error handling logic
  console.error(err.stack);
  res.status(400).send({error:'Could not decode request: JSON parsing failed'});
});

app.post('/',function(req,res){
	try{
		console.log(req.body);
		var list = req.body.payload;
		var halfway = [];
		for(var i = 0; i < list.length; i++) {
			if (list[i].episodeCount > 0 && list[i].drm == true){
				halfway.push({
					"image" : list[i].image.showImage,
					"slug"  : list[i].slug,
					"title" : list[i].title
				});
			}
		}
		res.send({"response":halfway});
	}
	catch(error){
		console.error(error);
		res.status(400).send({error: 'Could not decode request: JSON parsing failed'}); 
	}
});
